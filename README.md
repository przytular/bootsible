# Bootsible

Fedora Workstation provisioning with Ansible.

## Features
* ZSH
* Terminator
* Docker
* Desktop environment
* PyCharm



## Installation
```bash
$ ansible-pull
```